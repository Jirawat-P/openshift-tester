FROM node:10-alpine
LABEL name="NodeJs CICD" \
    version="1.0.0" \
    org.label-schema.vcs-url="https://gitlab.com/twin-opensource/nodejs-cicd" \
    org.label-schema.vendor="Twin synergy"
WORKDIR /usr/src/app
COPY ./ ./
RUN apk --no-cache add ca-certificates \
    && apk add --update tzdata \
    && cp /usr/share/zoneinfo/Asia/Bangkok /etc/localtime \
    && apk del tzdata
EXPOSE 8080
CMD [ "npm", "start" ]